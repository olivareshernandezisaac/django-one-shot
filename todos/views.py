from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id) #shows details of a specific object of the TodoList class
    context = {
        "list": list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=post)
    context = {
        "todolist_object": post,
        "todolist_form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            itemlist_form = form.save()                                # to find the specific item
            return redirect("todo_list_detail", itemlist_form.list.id) #we get the variable "list" of context in todolist_detail
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "todoitem_object": post,
        "todoitem_form": form,
    }
    return render(request, "todos/items/edit.html", context)
