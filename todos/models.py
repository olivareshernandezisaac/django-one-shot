from django.db import models

# Create your models here.

class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True)
    is_completed = models.BooleanField(default=False)

    list = models.ForeignKey(
        TodoList,
        related_name= "items",
        on_delete=models.CASCADE,
    )


#to access task and due_date from TodoList we will use the onetomany relationships
#TodoList.items.task to see all the tasks. sme with due_date
